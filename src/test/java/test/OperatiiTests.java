package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import model.Monom;
import model.Polinom;
import service.Operatii;

public class OperatiiTests {
	
	static Polinom p1;
	static Polinom p2;
	@BeforeClass
	public static void setup() {
		//System.out.println("before");
		ArrayList<Monom> m ;
		m = new ArrayList<>();
		m.add(new Monom(3, 1.0));
		m.add(new Monom(2, 0.0));
		m.add(new Monom(1,  -6.0));
		m.add(new Monom(0,  -16.0));
		p1 = new Polinom(m);
		m = new ArrayList<>();
		m.add(new Monom(1, 2.0));
		m.add(new Monom(0, -8.0));
		p2 = new Polinom(m);
	}
	
	@Test
	public void testAdunare() {
		Operatii op = new Operatii();
		ArrayList<Monom> m = new ArrayList<>();
		m.add(new Monom(3, 1.0));
		m.add(new Monom(1, -4.0));
		m.add(new Monom(0,  -24.0));
		Polinom expected = new Polinom(m);
		assertEquals(expected.toString(), op.aduna(p1,p2).toString());
	}
	
	@Test
	public void testScadere() {
		Operatii op = new Operatii();
		ArrayList<Monom> m = new ArrayList<>();
		m.add(new Monom(3, 1.0));
		m.add(new Monom(1, -8.0));
		m.add(new Monom(0,  -8.0));
		Polinom expected = new Polinom(m);
		assertEquals(expected.toString(), op.scade(p1,p2).toString());
	}
	@Test
	public void testInmultire() {
		Operatii op = new Operatii();
		ArrayList<Monom> m = new ArrayList<>();
		m.add(new Monom(4, 2.0));
		m.add(new Monom(3, -8.0));
		m.add(new Monom(2, -12.0));
		m.add(new Monom(1, 16.0));
		m.add(new Monom(0,  128.0));
		Polinom expected = new Polinom(m);
		assertEquals(expected.toString(), op.multiply(p1,p2).toString());
	}
	@Test
	public void testImpartire() {
		Operatii op = new Operatii();
		String s  = new String("rest : +24\ncat : +0.50x^2 +2x +5\n");
		System.out.println(op.divide(p1, p2));
		assertEquals(s, op.divide(p1, p2));
	}
	@Test
	public void testDerivare() {
		Operatii op = new Operatii();
		ArrayList<Monom> m = new ArrayList<>();
		m.add(new Monom(2, 3.0));
		m.add(new Monom(0, -6.0));
		Polinom expected = new Polinom(m);
		assertEquals(expected.toString(), op.differentiation(p1).toString());
	}
	@Test
	public void testIntegrare() {
		Operatii op = new Operatii();
		ArrayList<Monom> m = new ArrayList<>();
		m.add(new Monom(4, 0.25));
		m.add(new Monom(2, -3.0));
		m.add(new Monom(1, -16.0));
		Polinom expected = new Polinom(m);
		assertEquals(expected.toString(), op.integrate(p1).toString());
	}
	
	

}
