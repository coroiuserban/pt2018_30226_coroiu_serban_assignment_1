package service;

import model.*;
import java.util.ArrayList;
import java.util.Collections;


public class Operatii {



	public Polinom aduna(Polinom p1, Polinom p2) {
		Polinom s = null ;
		int gradM ;
		ArrayList<Monom> sm = new ArrayList<>();
		if(p1.getSize() > p2.getSize())
			gradM = p1.getGrad()+1;	
		else 
			gradM = p2.getGrad()+1;
		double[] rez = new double[gradM];
		for(Monom m : p1.getPolinom()) {
			rez[m.getExponent()]+= m.getCoeficient();
		}
		for(Monom m : p2.getPolinom()) {
			rez[m.getExponent()]+=m.getCoeficient();
		}
		int exp =0;
		for(Double d : rez) {
			sm.add(new  Monom(exp++, d));
		}
		Collections.reverse(sm);
		s = new Polinom(sm);
		if(s.getSizeCoef()==0)
			return null;
		else
			return s;
	}

	public Polinom scade(Polinom p1, Polinom p2) {
		Polinom s = null ;
		int gradM ;
		ArrayList<Monom> sm = new ArrayList<>();
		if(p1.getSize() > p2.getSize())
			gradM = p1.getGrad()+1;	
		else 
			gradM = p2.getGrad()+1;
		double[] dif = new double[gradM];
		for(Monom m :p1.getPolinom()) {
			dif[m.getExponent()]+=m.getCoeficient();
		}
		for(Monom m : p2.getPolinom()) {
			dif[m.getExponent()]-=m.getCoeficient();
		}
		int exp = 0;
		for(Double d : dif)
			sm.add(new Monom(exp++, d));
		Collections.reverse(sm);
		s = new Polinom(sm);
		
			return s;
	}

	public Polinom multiply(Polinom p1, Polinom p2) {
		Polinom s = null ;
		int grad = p1.getGrad()+p2.getGrad()+1;
		ArrayList<Monom> sm = new ArrayList<>();
		double[] rez = new double[grad];
		if(p1.getGrad() > p2.getGrad()) {//polinom p1 are mai multi coef ca si p2
			for(Monom m1 : p1.getPolinom()) 
				for(Monom m2 : p2.getPolinom())
					rez[m1.getExponent()+m2.getExponent()] += m1.getCoeficient()*m2.getCoeficient();
		}
		else {
			for(Monom m2 : p2.getPolinom()) 
				for(Monom m1 : p1.getPolinom())
					rez[m1.getExponent()+m2.getExponent()] += m1.getCoeficient()*m2.getCoeficient();
		}
		int exp=0;
		for(Double d : rez)	
			sm.add(new Monom(exp++, d));
		Collections.reverse(sm);
		s=new Polinom(sm);
		if(s.getSizeCoef()==0)
			return null;
		else
			return s;
		}
	
	public String divide(Polinom p1, Polinom p2) {
		ArrayList<Monom> sm = new ArrayList<>();
		Polinom cat = null;								//p1 stim ca are grad mai mare
		Polinom rest = null;
		rest = p1;
		for(int i=0;i<=p1.getGradN() - p2.getGradN();i++)				//creez polinomul in care se va stoca impartitorul la fiecare pas
			sm.add(new Monom(i, 0.0));
		Collections.reverse(sm);
		cat = new Polinom(sm);
		int k = 0;
		for(int j =p1.getSizeCoef(); j>=0 && p1.getSizeCoef() != 0 ;j--) {
			if(p1.getGradN() - p2.getGradN() >= 0) {
				sm = new ArrayList<>();									//reinitializam lista de monoame la fiecare pas
				int m = p1.getGradN() - p2.getGradN();
				for(int i=0;i<=m;i++)
					sm.add(new Monom(i,0.0));
				sm.add(new Monom(m, p1.getCoefMonom(k++)/p2.getCoefMonom(0)));
				Collections.reverse(sm);
				Polinom aux = new Polinom(sm);

				cat = aduna(aux, cat);
				aux = multiply(p2, aux);

				p1 = scade(p1, aux);
			}
		}
		StringBuilder rezultat = new StringBuilder("");		
		if(p1.getSizeCoef() != 0) {
			rest = scade(rest, multiply(cat, p2));
			rezultat.append("rest :" + rest);
		}
		rezultat.append("cat :" + cat);
		return rezultat.toString();
	}
		
	public Polinom differentiation(Polinom p1) {
		Polinom s = null ;
		ArrayList<Monom> sm = new ArrayList<>();
		double[] rez = new double[p1.getGrad()];
		if(p1.getGrad() >= 1)
			for(Monom m :p1.getPolinom()) {
				if(m.getCoeficient() != 0 && m.getExponent() >=1) {
					rez[m.getExponent()-1] = m.getCoeficient()*m.getExponent();
				}
			}
		else
			rez[0]=0;
		
		int exp=0;
		for(Double d : rez)	
			sm.add(new Monom(exp++, d));
		Collections.reverse(sm);
		s = new Polinom(sm);
		if(s.getSize()==0)
			return null;
		else
			return s;
	}
	
	public Polinom integrate(Polinom p) {
		Polinom s = null ;
		ArrayList<Monom> sm = new ArrayList<>();
		double[] rez  = new double[p.getSize()+1];
		if(p.getGrad() >= 0)
			for(Monom m : p.getPolinom()) {
				if(m.getCoeficient() != 0) {
					rez[m.getExponent()+1] = m.getCoeficient()/(m.getExponent()+1);
				}
			}
		int exp=0;
		for(Double d : rez) 
			sm.add(new Monom(exp++, d));
		Collections.reverse(sm);
		s = new Polinom(sm);
		if(s.getSize()==0) 
			return null;
		else
			return s;
	}
	
	
}

