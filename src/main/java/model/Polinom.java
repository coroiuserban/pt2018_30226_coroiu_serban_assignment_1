package model;

//______________________________________________________________________________
//|neaparat introducere semnului FARA SPATIU fata de x 	|
//|CONTEAZA ordinea de introducere 						|
//|	TREBUIE ca ordinea sa fie descrescatoare			|

import java.util.*;

public class Polinom {

	private ArrayList<Monom> polinom;

	public Polinom(ArrayList<Monom> polinom) {
		this.polinom = polinom;
	}

	public String toString() {
		StringBuilder s = new StringBuilder();
		for(Monom m : polinom)
			s.append(m);
		s.append("\n");
		return s.toString();
	}
		
	public int getSizeCoef() {
		int count=0;
		for(Monom m : polinom) {
			if(m.getCoeficient() != 0)
				count++;
		}
		return count;
	}
	
	public int getGrad() {
		return this.polinom.get(0).getExponent();
	}
	
	public int getSize() {
		return this.polinom.size();
	}
	
	public Monom getMonom(int i) {
		return this.polinom.get(i);
	}
	
	public double getCoefMonom(int i) {
		return this.polinom.get(i).getCoeficient();
	}
	
	private int getExpMonom(int i) {
		return this.polinom.get(i).getExponent();
	}
	
	public int getGradN() {
		int g=0;
		for(int i=this.polinom.size()-1;i>=0;i--)
			if(this.getCoefMonom(i) != 0)
				g=this.getExpMonom(i);
		return g;
	}
	
	public void negarePolinom() {
		for(Monom m: polinom)
			if(m.getCoeficient()!= 0)
				m.setCoeficient(-1*m.getCoeficient());
	}

	public ArrayList<Monom> getPolinom(){
		return polinom;
	}
}
 