package model;

public class Monom {
	private Double coeficient;
	private int exponent;
	public Monom() {
		this.coeficient = 0.0;
		this.exponent = 0;
	}
	public Monom(int exponent, Double coeficient) {
		this.coeficient = coeficient;
		this.exponent = exponent;
	}
	public void setCoeficient(Double coeficient) {
		this.coeficient = coeficient;
	}
	public void setExponent(int exponent) {
		this.exponent = exponent;
	}
	public Double getCoeficient() {
		return coeficient;
	}
	public int getExponent() {
		return exponent; 
	}
	public String toString() {
		StringBuilder s = new StringBuilder();
			if(coeficient != 0.0) {
				if(coeficient != Math.floor(coeficient)) {
					if(coeficient > 1) {
						s.append(" +");s.append(String.format("%.2f",coeficient));}
					else
						if(coeficient >= 0) {
							s.append(" +");s.append(String.format("%.2f",coeficient));}
						else
							if(coeficient == -1)
								s.append(String.format("%.2f", coeficient));
							else
								s.append(String.format("%.2f", coeficient));
				}else {
					if(coeficient > 1)
						s.append(" +" + coeficient.intValue());
					else
						if(coeficient == 1)
							if(coeficient == 1 && exponent==0)
								s.append(" +" + coeficient.intValue());
							else
								s.append(" +");
						else
							if(coeficient == -1)
								if(coeficient == -1 && exponent==0)
									s.append(coeficient.intValue());
								else
									s.append(" -");
							else
								s.append(" "+ coeficient.intValue());
				}
				//-----------------------------------------------------------------------------------------------
				if(exponent > 1)
					s.append("x^" + exponent);
				else 
					if(exponent == 1) 
						s.append("x");
			}
		return s.toString();
	}

}
 