package gui;



import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import model.*;
import service.*;

public class App {

	public App() {
		Create();
	}

	private ArrayList<Monom> m1;
	private ArrayList<Monom> m2;
	private Polinom p1 = null;
	private Polinom p2 = null;
	private Operatii op = new Operatii();
	private String r;
	
	private void Create() {
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Calculator Polinoame");
		frame.setBounds(400, 100, 440, 315);//440, 280
		frame.setLayout(null);
		frame.setResizable(false);
		
		JPanel panel1 = new JPanel();
		panel1.setBounds(10, 10, 480, 380);
		panel1.setLayout(null);

		//pt f(x)------------------------------------------------------
		JLabel labelf = new JLabel("f(x) = ");
		labelf.setBounds(10, 20, 40, 20);
		panel1.add(labelf);

		JTextField textf = new JTextField();
		textf.setBounds(50, 15, 220, 30);
		textf.setText("x^2 -6x^1 -16x^0");
		panel1.add(textf);

		JButton buttonf = new JButton("Enter f(x)");
		buttonf.setBounds(280, 15, 120, 25);
		panel1.add(buttonf);

		//pt g(x)------------------------------------------------------
		JLabel labelg = new JLabel("g(x) = ");
		labelg.setBounds(10, 60, 40, 20);
		panel1.add(labelg);

		JTextField textg = new JTextField();
		textg.setBounds(50, 55, 220, 30);
		textg.setText("x^1 -8x^0");
		panel1.add(textg);

		JButton buttong = new JButton("Enter g(x)");
		buttong.setBounds(280, 55, 120, 25);
		panel1.add(buttong);
		
		//afisare rezultat----------------------------------------------
		JPanel j = new JPanel();
		j.setBounds(60, 180, 250, 200);
		panel1.add(j);
		
		JTextArea rez = new JTextArea();
		JScrollPane scroll = new JScrollPane(rez);
		rez.setLineWrap(true);
		rez.setWrapStyleWord(true);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setPreferredSize(new Dimension(225, 80));
		j.add(scroll);

		//butoane operatii

		JButton add =  new JButton("+");
		add.setBounds(10, 110, 90, 25);
		panel1.add(add);

		JButton substract =  new JButton("-");
		substract.setBounds(115, 110, 90, 25);
		panel1.add(substract);

		JButton multiply =  new JButton("*");
		multiply.setBounds(220, 110, 90, 25);
		panel1.add(multiply);

		JButton divide =  new JButton("/");
		divide.setBounds(325, 110, 90, 25);
		panel1.add(divide);

		//operatii pe un singur polinom
		JButton derivaref =  new JButton("Derivare f");
		derivaref.setBounds(8, 145, 95, 25);
		panel1.add(derivaref);

		JButton derivareg =  new JButton("Derivare g");
		derivareg.setBounds(113, 145, 95, 25);
		panel1.add(derivareg);

		JButton integraref =  new JButton("Integrare f");
		integraref.setBounds(218, 145, 95, 25);
		panel1.add(integraref);
 
		JButton integrareg =  new JButton("Integrare g");
		integrareg.setBounds(323, 145, 96, 25);
		panel1.add(integrareg);
		//ActionListener pt butoanele de submit -----------------------------

		buttonf.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				m1 = new ArrayList<>();
				try {
					String string = textf.getText();
					String[] spaceSplit = string.split(" ");
					String[] xSplit = null ;
					xSplit = spaceSplit[0].split("x\\^");
					int gradM = Integer.parseInt(xSplit[1]);
					double[] coef = new double[50];
					for(String s : spaceSplit) {
						xSplit = s.split("x\\^");
						try{
							coef[Integer.parseInt(xSplit[1])] = Double.parseDouble(xSplit[0]);
						}catch(NumberFormatException e1) {
							if(xSplit[0].contains("-")) 
								coef[Integer.parseInt(xSplit[1])] = -1.0;
							else
								coef[Integer.parseInt(xSplit[1])] = 1.0;
							xSplit[0] = xSplit[0].concat("1");
						}
					}
					for(int i=gradM;i>=0;i--)
						m1.add(new Monom(i, coef[i]));
					p1 = new Polinom(m1);
				}catch(Exception e1) {
					rez.append("error : f\n");
				}
			}
		});
		
		buttong.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					m2 = new ArrayList<>();
					String string = textg.getText();
					String[] spaceSplit = string.split(" ");
					String[] xSplit = null ;

					xSplit = spaceSplit[0].split("x\\^");
					int gradM = Integer.parseInt(xSplit[1]);
					double[] coef = new double[50];

					for(String s : spaceSplit) {
						xSplit = s.split("x\\^");
						try{
							coef[Integer.parseInt(xSplit[1])] = Double.parseDouble(xSplit[0]);
						}catch(NumberFormatException e1) {
							if(xSplit[0].contains("-")) 
								coef[Integer.parseInt(xSplit[1])] = -1.0;
							else
								coef[Integer.parseInt(xSplit[1])] = 1.0;
							xSplit[0] = xSplit[0].concat("1");
						}
					}
					for(int i=gradM;i>=0;i--)
						m2.add(new Monom(i, coef[i]));
					p2 = new Polinom(m2);
				}catch(Exception e1) {
					rez.append("error : g\n");
				}
			}
		});

		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					p1 = op.aduna(p1, p2);
					if(p1==null)
						rez.append("0");
					else
						rez.append(p1.toString());
				}catch (Exception e1) {
					rez.append("Error +");
				}
			}
		});

		substract.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					p1 = op.scade(p1, p2);
					
					if(p1.getSizeCoef() == 0)
						rez.append("0");
					else
						rez.append(p1.toString());
				}catch (Exception e1) {
					rez.append("Error -");		
				}	
			}
		});

		multiply.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					p1 = op.multiply(p1, p2);
					rez.append(p1.toString());
				}catch (Exception e1) {
					if(p2.getSizeCoef()==0)
						rez.append("0\n");
					else
						rez.append("null Error\n");
				}
			}
		});
		
		divide.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if(p1.getGrad() < p2.getGrad())
						throw new Exception("f are gradul mai mic ca si g");
					else {
						r=op.divide(p1, p2); 
						rez.append(r);
					}
				} catch (Exception e1) {
					rez.append("null Error\n");
				}
			}
		});
		
		derivaref.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					p1 = op.differentiation(p1);
					if(p1==null)
						rez.append("0");
					else
						rez.append(p1.toString());
				}catch (Exception e1) {
					rez.append("Nu se poate deriva 0!\n");
				}
			}
		});

		derivareg.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					p2 = op.differentiation(p2);
					if(p2==null)
						rez.append("0");
					else
						rez.append(p2.toString());
				}catch (Exception e1) {
					rez.append("Nu se poate deriva 0!\n");
				}
				
			}
			
		});

		integraref.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				p1 = op.integrate(p1);
				rez.append(p1.toString());
			}
		});
		
		integrareg.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				p2 = op.integrate(p2);
				rez.append(p2.toString());
				
			}
		});
		
		frame.setContentPane(panel1);
		frame.setVisible(true);
	}
}
